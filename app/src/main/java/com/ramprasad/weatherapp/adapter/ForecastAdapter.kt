package com.ramprasad.weatherapp.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ramprasad.weatherapp.databinding.ForecastListBinding
import com.ramprasad.weatherapp.data.WeatherResponse
import java.text.SimpleDateFormat
import java.util.*
import javax.annotation.Nullable


/**
 * Created by Ramprasad on 6/3/23.
 */
class ForecastAdapter(private val forecastListData: MutableList<WeatherResponse> = mutableListOf()) :
    RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {


    fun setForecastList(newDataList: List<WeatherResponse>) {
        forecastListData.clear()
        //forecastListData.addAll(newDataList)
        // notifyDataSetChanged()
        val diffResult: DiffUtil.DiffResult =
            DiffUtil.calculateDiff(ForecastListDiffCallback(forecastListData, newDataList))
        forecastListData.addAll(newDataList)
        diffResult.dispatchUpdatesTo(this)
    }

    /*    private val diffUtilCallback = object : DiffUtil.Callback<ForecastList>() {
            override fun areItemsTheSame(oldItem: ForecastList, newItem: ForecastList): Boolean {
                return oldItem.dt == newItem.dt
            }

            override fun areContentsTheSame(oldItem: ForecastList, newItem: ForecastList): Boolean {
                return oldItem == newItem
            }

        }
        private val differ = AsyncListDiffer(this, diffUtilCallback)*/

    class ForecastListDiffCallback(
        private val oldList: List<WeatherResponse>,
        private val newList: List<WeatherResponse>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].dt === newList[newItemPosition].dt
        }

        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            return when {
                oldList[oldPosition].dt != newList[newPosition].dt -> {
                    false
                }

                oldList[oldPosition].clouds != newList[newPosition].clouds -> {
                    false
                }

                else -> true
            }
        }

        @Nullable
        override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
            return super.getChangePayload(oldPosition, newPosition)
        }
    }


    class ForecastViewHolder(private val forecastListBinding: ForecastListBinding) :
        RecyclerView.ViewHolder(forecastListBinding.root) {
        fun bind(forecast: WeatherResponse) {
            // Format String to date (Example: Mon May 29 09:00:00 PDT 2023)
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val date = forecast.dt?.let { format.parse(it.toString()) }

            // Format time
            val formatTime = SimpleDateFormat("HH:mm a", Locale.getDefault())

            Log.e("ForecastIcon", forecast.weather[0].icon.toString())
            Glide.with(forecastListBinding.root.context)
                .load("https://openweathermap.org/img/wn/" + forecast.weather[0].icon + ".png")
                .into(forecastListBinding.weatherConditionIcon)


            // set day
            val formatDate = SimpleDateFormat("EEEE", Locale.getDefault())




            forecastListBinding.countryName.text = forecast.main?.temp.toString()
            forecastListBinding.apply {

//                if (date != null) {
//                    Log.e("ForecastDate", date.toString())
//                    tvTemperature.text = date.toString()
//                }

                Log.e("ForecastTime", date.toString())
                Log.e("ForecastIccon", forecast.weather[0].icon.toString())
                tvTemperature.text = forecast.weather[0].icon.toString()
                tvTime.text = date?.let { formatTime.format(it) }
                tvDay.text = date?.let { formatDate.format(it) }

            }





            forecastListBinding.countryCardView.setOnClickListener {
                Toast.makeText(
                    forecastListBinding.root.context,
                    "Clicked on the Country: " + " " + forecast.wind.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder =
        ForecastViewHolder(
            ForecastListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount(): Int = forecastListData.size


    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        holder.bind(forecastListData[position])
    }


}
