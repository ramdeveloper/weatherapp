package com.ramprasad.weatherapp.data

import android.os.Parcelable
//import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sys(
    val country: String,
    val sunrise: Int,
    val sunset: Int
) : Parcelable
