package com.ramprasad.weatherapp.data

import android.os.Parcelable
//import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.Parcelize

@Parcelize
data class Weathers(
    val cnt: Int,
    val list: List<WeatherResponse>
) : Parcelable
