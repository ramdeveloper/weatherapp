package com.ramprasad.weatherapp.viewmodel

import com.ramprasad.weatherapp.data.WeatherResponse
import com.ramprasad.weatherapp.data.Weathers
import com.ramprasad.weatherapp.network.StateOfRespose
import com.ramprasad.weatherapp.repositories.CountryRepository
import com.ramprasad.weatherapp.repositories.WeatherRepository
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WeatherDataViewModel
@Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val countryRepository: CountryRepository
) : ViewModel() {

    private var singleWeather_ = MutableLiveData<StateOfRespose<out WeatherResponse?>>(null)
    var singleWeather: LiveData<StateOfRespose<out WeatherResponse?>> = singleWeather_

    private var weathersList_ = MutableLiveData<StateOfRespose<out Weathers?>>(null)
    var weathersList: LiveData<StateOfRespose<out Weathers?>> = weathersList_

    suspend fun getCountries() = countryRepository.getCountries()

    suspend fun getWeathers(citiesIds: String) {
        weatherRepository.getWeathersList(citiesIds).collect { result ->
            weathersList_.postValue(result)
        }
    }


    suspend fun getWeatherByCityName(cityName: String) {
        weatherRepository.getWeatherByCityName(cityName).collect { result ->
            singleWeather_.postValue(result)
        }
    }


    suspend fun getWeatherByLatLon(lat: String, lon: String) {
        weatherRepository.getWeatherByLatLon(lat, lon).collect { result ->
            when (result) {
                is StateOfRespose.Success -> {
                    result.data?.isCurrentLocation = true
                    singleWeather_.postValue(result)
                }
                else -> {
                    singleWeather_.postValue(result)
                }
            }
        }
    }

}
