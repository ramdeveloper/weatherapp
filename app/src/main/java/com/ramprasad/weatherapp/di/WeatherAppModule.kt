package com.ramprasad.weatherapp.di

import android.app.Application
import com.ramprasad.weatherapp.db.CountryAppDatabase
import com.ramprasad.weatherapp.db.CountryAppDatabase.Companion.DATABASE_NAME
import com.ramprasad.weatherapp.db.CountriesDao
import com.ramprasad.weatherapp.location.LocationManagerService
import com.ramprasad.weatherapp.network.WeatherServiceApi
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object WeatherAppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(WeatherServiceApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideWeatherService(retrofit: Retrofit): WeatherServiceApi =
        retrofit.create(WeatherServiceApi::class.java)

    @Provides
    @Singleton
    fun provideDatabase(app: Application): CountryAppDatabase =
        Room.databaseBuilder(app, CountryAppDatabase::class.java, DATABASE_NAME)
            .createFromAsset("database/country.db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideLocationManagerService(): LocationManagerService = LocationManagerService()

    @Provides
    @Singleton
    fun provideCountryDao(database: CountryAppDatabase): CountriesDao = database.countriesDao()
}
