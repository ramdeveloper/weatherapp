package com.ramprasad.weatherapp.di

import com.ramprasad.weatherapp.data.Country

object CountryData {

    fun getCountryList() = listOf(Country("123"), Country("321"), Country("444"))
}
