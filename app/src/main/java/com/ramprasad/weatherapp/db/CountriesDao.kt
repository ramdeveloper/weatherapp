package com.ramprasad.weatherapp.db

import com.ramprasad.weatherapp.data.COUNTRY_TABLE
import com.ramprasad.weatherapp.data.Country
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


//Room DB
@Dao
interface CountriesDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertCountry(country: Country)

    @Query("SELECT * FROM $COUNTRY_TABLE")
    suspend fun getCountries(): List<Country>?
}
