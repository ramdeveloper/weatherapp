package com.ramprasad.weatherapp.db

import com.ramprasad.weatherapp.data.Country
import javax.inject.Inject

class CountryDataSource @Inject constructor(
    private val countriesDao: CountriesDao
) {
    suspend fun insertCountry(country: Country) =
        countriesDao.insertCountry(country)

    suspend fun getCountries(): List<Country>? =
        countriesDao.getCountries()
}
