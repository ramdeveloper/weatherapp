package com.ramprasad.weatherapp.db

import com.ramprasad.weatherapp.data.Country
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Country::class], version = 1)
abstract class CountryAppDatabase : RoomDatabase() {

    abstract fun countriesDao(): CountriesDao

    companion object {
        const val DATABASE_NAME = "weather_database"
    }
}
