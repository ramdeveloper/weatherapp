package com.ramprasad.weatherapp.ui.weather


import com.ramprasad.weatherapp.data.WeatherResponse
import com.ramprasad.weatherapp.util.DateUtils.getCurrentDay
import com.ramprasad.weatherapp.util.DateUtils.getDayOfWeek
import com.ramprasad.weatherapp.util.removeNonSpacingMarks
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ramprasad.weatherapp.R
import com.ramprasad.weatherapp.databinding.WeatherItemBinding
import kotlinx.coroutines.Job
import java.util.Calendar
import java.util.Locale
import kotlin.math.roundToInt

class CountryWeatherAdapter(
    private var weatherList: ArrayList<WeatherResponse?>,
    private val navigateToDetail: (weatherResponse: WeatherResponse) -> Unit,
    private val searchWeatherByCityName: (cityName: String) -> Unit,
    private val cleanSearchView: () -> Unit
) : RecyclerView.Adapter<CountryWeatherAdapter.CountryAdapterViewHolder>() {

    var weatherCoroutineScope: Job? = null

    private val oldWeathersList: ArrayList<WeatherResponse> = arrayListOf()

    private lateinit var countryWeatherRecyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        countryWeatherRecyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryAdapterViewHolder {
        val binding = WeatherItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return CountryAdapterViewHolder(binding, navigateToDetail)
    }

    override fun onBindViewHolder(holder: CountryAdapterViewHolder, position: Int) {
        val currentItem = weatherList[position]
        holder.bind(currentItem)
    }

    fun updateData(weathersList: List<WeatherResponse>) {
        val alreadyContains = oldWeathersList.toSet().containsAll(weathersList.toSet())

        if (!alreadyContains) {
            val difference = weathersList.toSet().minus(oldWeathersList.toSet())
            weatherList.addAll(difference)
            oldWeathersList.addAll(difference)
            notifyDataSetChanged()
        }
    }

    fun addSingleWeather(weatherResponse: WeatherResponse?) {
        weatherResponse?.let {
            val result = oldWeathersList.filter {
                it.id == weatherResponse.id
            }

            if (result.isEmpty()) {
                weatherList.add(0, weatherResponse)
                notifyItemInserted(0)
                countryWeatherRecyclerView.scrollToPosition(0)
                oldWeathersList.add(0, weatherResponse)
            }
        }
    }

    fun getFilter(): Filter {
        return countryWeatherFilter
    }

    private val countryWeatherFilter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            weatherCoroutineScope?.cancel()

            val filteredList: ArrayList<WeatherResponse> = arrayListOf()
            if (constraint == null || constraint.isEmpty()) {
                oldWeathersList.let { filteredList.addAll(it) }
            } else {
                val query = constraint.toString().trim().toLowerCase().removeNonSpacingMarks()
                oldWeathersList.forEach {
                    if (it.name.toLowerCase(Locale.ROOT).removeNonSpacingMarks().contains(query)) {
                        filteredList.add(it)
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            if (results?.values is ArrayList<*>) {
                weatherList.clear()
                weatherList.addAll(results.values as ArrayList<WeatherResponse>)
                notifyDataSetChanged()

                if (weatherList.isEmpty()) {
                    searchWeatherByCityName(constraint.toString())
                } else {
                    cleanSearchView()
                }
            }
        }
    }

    class CountryAdapterViewHolder(
        private val binding: WeatherItemBinding,
        private val navigateToDetail: (weatherResponse: WeatherResponse) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(weatherResponse: WeatherResponse?) {
            binding.apply {
                weatherResponse?.let { weatherObject ->
                    root.setOnClickListener {
                        navigateToDetail(weatherObject)
                    }
                    tvCity.text = if (weatherObject.isCurrentLocation)
                        binding.root.context.getString(R.string.city_name_current_location, weatherObject.name, weatherObject.sys.country)
                    else
                        binding.root.context.getString(R.string.city_name, weatherObject.name, weatherObject.sys.country)
                    tvDegree.text = binding.root.context.getString(R.string.degree_detail,
                        weatherObject.main.temp.roundToInt().toString().plus(0x00B0.toChar()))
                    tvDesc.text = weatherObject.weather[0].description.replaceFirstChar {
                        if (it.isLowerCase()) it.titlecase(Locale.getDefault())
                        else it.toString() }
                    tvHumidity.text = weatherObject.main.humidity.toString()
                    val calendar: Calendar = Calendar.getInstance()
                    calendar.timeZone.rawOffset = 0
                    val offset = if (weatherObject.timezone != 0) 3600 else 0
                    calendar.timeInMillis = calendar.timeInMillis + ((weatherObject.timezone - offset) * 1000).toLong()
                    tvDay.text = getCurrentDay(calendar.time)
                    tvDayName.text = getDayOfWeek(calendar.time)

                    Glide.with(itemView)
                        .load("https://openweathermap.org/img/wn/" + weatherObject.weather[0].icon + ".png")
                        .into(imageWeather)
                } ?: run {
                    tvCity.text = binding.root.context.getString(R.string.unable_to_fetch_data)
                }
            }
        }
    }

    override fun getItemCount(): Int = weatherList.size
}
