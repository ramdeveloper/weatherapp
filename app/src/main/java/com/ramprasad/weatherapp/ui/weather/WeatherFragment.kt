package com.ramprasad.weatherapp.ui.weather

import android.os.Bundle
import com.ramprasad.weatherapp.data.WeatherResponse
import com.ramprasad.weatherapp.network.StateOfRespose
import android.view.* // ktlint-disable no-wildcard-imports
import android.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.* // ktlint-disable no-wildcard-imports
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ramprasad.weatherapp.databinding.FragmentWeatherListBinding
import com.ramprasad.weatherapp.viewmodel.WeatherDataViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WeatherFragment : Fragment() {

    private val viewModel: WeatherDataViewModel by activityViewModels()
    private var currentSearchCityName: String? = null
    private val weatherList: ArrayList<WeatherResponse?> = arrayListOf()
    private lateinit var binding: FragmentWeatherListBinding

    private val adapter = CountryWeatherAdapter(
        weatherList,
        navigateToDetail = { weatherObject -> navigateToDetail(weatherObject) },
        searchWeatherByCityName = { cityName -> searchWeatherByCityName(cityName) },
        cleanSearchView = { cleanSearchView() }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        View Binding
        binding = FragmentWeatherListBinding.inflate(layoutInflater)

        binding.recyclerViewPhoneList.adapter = adapter
        binding.recyclerViewPhoneList.layoutManager = LinearLayoutManager(context)

        binding.btSearch.setOnClickListener {
            val weatherCoroutineScope = CoroutineScope(Dispatchers.Main).launch {
                currentSearchCityName?.let {
                    binding.tvError.isVisible = true
                    binding.tvError.text = "Fetching weather Based on Location"
                    viewModel.getWeatherByCityName(it)
                }
                if (!isActive) {
                    binding.btSearch.isVisible = false
                }
            }
            adapter.weatherCoroutineScope = weatherCoroutineScope
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                adapter.getFilter().filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.getFilter().filter(newText)
                return false
            }
        })

        viewModel.singleWeather.observe(
            viewLifecycleOwner,
            Observer { result ->
                when (result) {
                    is StateOfRespose.Success -> {
                        binding.tvError.isVisible = false
                        binding.progressBar2.isVisible = false
                        adapter.addSingleWeather(result.data)
                    }

                    is StateOfRespose.Error -> {
                        binding.progressBar2.isVisible = false
                        binding.tvError.text = "Not able to fetch Weather"
                        binding.tvError.isVisible = true
                    }

                    else -> {
                        binding.tvError.isVisible = true
                        binding.tvError.text = "Hold On Loading..."
                    }
                }
            }
        )

        viewModel.weathersList.observe(
            viewLifecycleOwner,
            Observer { result ->
                when (result) {
                    is StateOfRespose.Success -> {
                        binding.tvError.isVisible = false
                        adapter.updateData(result.data?.list ?: listOf())
                    }
                    is StateOfRespose.Error -> {
                        binding.tvError.text = result.toString()
                        binding.tvError.isVisible = true
                    }
                    else -> {
                        binding.tvError.isVisible = true
                        binding.tvError.text = "Hold On Loading..."
                    }
                }
            }
        )
        return binding.root
    }



    private fun navigateToDetail(weatherResponse: WeatherResponse) {
        findNavController().navigate(WeatherFragmentDirections.actionWeatherFragmentToWeatherDetailsFragment(weatherResponse))
    }


    private fun searchWeatherByCityName(cityName: String) {
        currentSearchCityName = cityName
        binding.btSearch.isVisible = true
    }

    private fun cleanSearchView() {
        binding.btSearch.isVisible = false
        binding.tvError.isVisible = false
    }
}
