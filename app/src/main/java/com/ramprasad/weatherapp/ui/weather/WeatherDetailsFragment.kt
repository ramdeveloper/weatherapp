package com.ramprasad.weatherapp.ui.weather


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ramprasad.weatherapp.util.DateUtils.getCurrentDay
import com.ramprasad.weatherapp.util.DateUtils.getDayOfWeek
import com.ramprasad.weatherapp.util.DateUtils.getTime
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.ramprasad.weatherapp.R
import com.ramprasad.weatherapp.databinding.FragmentWeatherDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import java.util.Calendar
import java.util.Locale
import kotlin.math.roundToInt

@AndroidEntryPoint
class WeatherDetailsFragment : Fragment() {

    private val args: WeatherDetailsFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentWeatherDetailBinding.inflate(layoutInflater)


        args.weatherObject.let {

            binding.apply {

                tvDayDetail.text = it.weather[0].description.replaceFirstChar { word->
                    if (word.isLowerCase()) word.titlecase(Locale.getDefault())
                    else word.toString()
                }

                Glide.with(tvWeatherIcon)
                    .load("https://openweathermap.org/img/wn/" + it.weather[0].icon + ".png")
                    .into(tvWeatherIcon)

                tvCityName.text = if (it.isCurrentLocation) getString(
                    R.string.city_name_current_location,
                    it.name, it.sys.country
                ) else getString(R.string.city_name, it.name,  it.sys.country)
                tvMaximumMinimum.text = getString(
                    R.string.temp_max_min,
                    it.main.temp_max.roundToInt().toString(),
                    it.main.temp_min.roundToInt().toString()
                )
                tvDetailDegree.text =
                    getString(
                        R.string.degree_detail,
                        it.main.temp.roundToInt().toString().plus(0x00B0.toChar())
                    )
                tvFeelsLike.text = it.main.feels_like.toString().plus(0x00B0.toChar())
                tvWindSpeed.text = it.wind.speed.toString().plus(" mt/s")
                tvHumidity.text = it.main.humidity.toString()
                val calendar: Calendar = Calendar.getInstance()
                calendar.timeZone.rawOffset = 0
                val offset = if (it.timezone != 0) 3600 else 0
                calendar.timeInMillis =
                    calendar.timeInMillis + ((it.timezone - offset) * 1000).toLong()
                tvDayDetail.text = getCurrentDay(calendar.time)
                tvDayWeekDetail.text = getDayOfWeek(calendar.time)
                tvHour.text = getTime(calendar.time)

            }
        }


        return binding.root



    }

}
