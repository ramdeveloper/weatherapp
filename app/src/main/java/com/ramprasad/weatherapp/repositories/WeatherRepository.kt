package com.ramprasad.weatherapp.repositories

import com.ramprasad.weatherapp.network.WeatherDataSource
import javax.inject.Inject

class WeatherRepository @Inject constructor(
    private val dataSource: WeatherDataSource
) {
    suspend fun getWeatherByCityName(cityName: String) =
        dataSource.getWeatherByCityName(cityName)

    suspend fun getWeatherByCityNameCountryCode(cityName: String, countryCode: String) =
        dataSource.getWeatherByCityNameCountryCode(cityName, countryCode)

    suspend fun getWeathersList(citiesIds: String) =
        dataSource.getWeathersList(citiesIds)

    suspend fun getWeatherByLatLon(lat: String, lon: String) =
        dataSource.getWeatherByLatLon(lat, lon)

}
