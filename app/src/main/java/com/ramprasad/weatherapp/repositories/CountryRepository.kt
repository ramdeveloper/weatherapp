package com.ramprasad.weatherapp.repositories

import com.ramprasad.weatherapp.db.CountryDataSource
import com.ramprasad.weatherapp.data.Country
import javax.inject.Inject

class CountryRepository @Inject constructor(
    private val dataSource: CountryDataSource
) {

    suspend fun getCountries(): List<Country>? {
        return dataSource.getCountries()
    }

    suspend fun insertCountry(country: Country) {
        dataSource.insertCountry(country)
    }
}
