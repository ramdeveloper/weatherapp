package com.ramprasad.weatherapp.network

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

suspend fun <T> request(event: suspend () -> Response<T>): Flow<StateOfRespose<out T?>> {
    return flow {
        try {
            emit(StateOfRespose.Loading)
            val response: Response<T> = event()

            if (response.isSuccessful) {
                emit(StateOfRespose.Success(data = response.body()!!))
            } else {
                emit(StateOfRespose.Error(errorMessage = response.message() ?: "Something went wrong"))
            }
        } catch (e: HttpException) {
            emit(StateOfRespose.Error(errorMessage = e.message ?: "Something went wrong"))
        } catch (e: IOException) {
            emit(StateOfRespose.Error("Please check your network connection"))
        } catch (e: Exception) {
            emit(StateOfRespose.Error(errorMessage = e.message ?: "Something went wrong"))
        }
    }
}
