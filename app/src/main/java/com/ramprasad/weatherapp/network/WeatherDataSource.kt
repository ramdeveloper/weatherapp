package com.ramprasad.weatherapp.network

import javax.inject.Inject

class WeatherDataSource @Inject constructor(
    private val weatherServiceApi: WeatherServiceApi
) {
    suspend fun getWeatherByCityName(cityName: String) =
        request {
            weatherServiceApi.getWeatherByCityName(cityName)
        }

    suspend fun getWeatherByCityNameCountryCode(cityName: String, countryCode: String) =
        request {
            weatherServiceApi.getWeatherByCityNameCountryCode(cityName+','+countryCode)
        }

    suspend fun getWeathersList(cityNames: String) =
        request {
            weatherServiceApi.getWeathersList(cityNames)
        }

    suspend fun getWeatherByLatLon(lat: String, lon: String) =
        request {
            weatherServiceApi.getWeatherByLatLon(lat, lon)
        }

    /*suspend fun getForecastWeather(lat: String, lon: String) =
        request {
            weatherService.getForecastWeather()
        }*/

}
