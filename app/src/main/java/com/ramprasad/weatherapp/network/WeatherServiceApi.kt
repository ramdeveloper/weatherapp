package com.ramprasad.weatherapp.network

import com.ramprasad.weatherapp.data.WeatherResponse
import com.ramprasad.weatherapp.data.Weathers
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherServiceApi {

    companion object {
        const val BASE_URL = "https://api.openweathermap.org/"
        const val API_KEY = "23096676a202def122e115d3c1120241"
        private const val FORECAST_WEATHER =
            "forecast?lat=44.34&lon=10.99&appid=23096676a202def122e115d3c1120241&units=metric"
    }

    @GET("data/2.5/group")
    suspend fun getWeathersList(
        @Query("id") citiesIds: String,
        @Query("appid") appid: String = API_KEY,
        @Query("units") units: String = "metric"
    ): Response<Weathers>

    @GET("data/2.5/weather")
    suspend fun getWeatherByLatLon(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("appid") appid: String = API_KEY,
        @Query("units") units: String = "metric"
    ): Response<WeatherResponse>

    @GET("data/2.5/weather")
    suspend fun getWeatherByCityName(
        @Query("q") citiesIds: String,
        @Query("appid") appid: String = API_KEY,
        @Query("units") units: String = "metric"
    ): Response<WeatherResponse>

    @GET("data/2.5/weather")
    suspend fun getWeatherByCityNameCountryCode(
        @Query("q") citiesIds: String,
        @Query("appid") appid: String = API_KEY,
        @Query("units") units: String = "metric"
    ): Response<WeatherResponse>

    @GET(FORECAST_WEATHER)
    suspend fun getForecastWeather(): Response<Weathers>
}
