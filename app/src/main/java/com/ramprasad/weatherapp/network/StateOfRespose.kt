package com.ramprasad.weatherapp.network

sealed class StateOfRespose<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T) : StateOfRespose<T>(data = data)
    class Error<T>(errorMessage: String) : StateOfRespose<T>(message = errorMessage)
    object Loading : StateOfRespose<Nothing>()
}
