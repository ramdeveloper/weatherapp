package com.ramprasad.weatherapp.utils

import com.ramprasad.weatherapp.data.Clouds
import com.ramprasad.weatherapp.data.Coord
import com.ramprasad.weatherapp.data.WeatherResponse
import com.ramprasad.weatherapp.data.Weathers
import com.ramprasad.weatherapp.data.Main
import com.ramprasad.weatherapp.data.Sys
import com.ramprasad.weatherapp.data.Weather
import com.ramprasad.weatherapp.data.Wind
import com.ramprasad.weatherapp.network.StateOfRespose

/**
 * Created by Ramprasad on 6/7/23.
 */
object WeatherFactory {
    fun createWeatherListNetworkResult(): StateOfRespose<Weathers> {
        return StateOfRespose.Success(
            createWeatherList
        )
    }

    fun createWeatherNetworkResult(): StateOfRespose<WeatherResponse> {
        return StateOfRespose.Success(
            createWeatherList.list[0]
        )
    }

    val createWeatherList = Weathers(
        cnt = 0,
        list = listOf(
            WeatherResponse(
                isCurrentLocation = true,
                clouds = Clouds(all = 0),
                coord = Coord(lat = 38.7167, lon = -9.1333),
                dt = 1660759785,
                id = 2267057,
                main = Main(
                    feels_like = 21.21, grnd_level = 0, humidity = 50, pressure = 1016,
                    sea_level = 0, temp = 21.68, temp_max = 22.68, temp_min = 20.86
                ),
                name = "Lisbon",
                sys = Sys(country = "PT", sunrise = 1660715531, sunset = 1660764563),
                visibility = 10000,
                weather = listOf(
                    Weather(description = "clear sky", icon = "01d", id = 800, main = "Clear")
                ),
                wind = Wind(deg = 360, speed = 13.89),
                timezone = 0
            ),
            WeatherResponse(
                isCurrentLocation = false,
                clouds = Clouds(all = 40),
                coord = Coord(lat = 40.4165, lon = -3.7026),
                dt = 1660759779,
                id = 3117735,
                main = Main(
                    feels_like = 24.55, grnd_level = 0, humidity = 31,
                    pressure = 1013, sea_level = 0, temp = 25.17, temp_max = 26.79, temp_min = 22.78
                ),
                name = "Madrid",
                sys = Sys(country = "ES", sunrise = 1660714052, sunset = 1660763436),
                visibility = 10000,
                weather = listOf(
                    Weather(
                        description = "scattered clouds",
                        icon = "03d", id = 802, main = "Clouds"
                    )
                ),
                wind = Wind(deg = 80, speed = 3.6),
                timezone = 0
            ),
            WeatherResponse(
                isCurrentLocation = false, clouds = Clouds(all = 75),
                coord = Coord(lat = 48.8534, lon = 2.3488), dt = 1660759785,
                id = 2988507, main = Main(
                    feels_like = 22.52, grnd_level = 0,
                    humidity = 74, pressure = 1011, sea_level = 0, temp = 22.3,
                    temp_max = 26.34, temp_min = 19.88
                ), name = "Paris",
                sys = Sys(
                    country = "FR", sunrise = 1660711558,
                    sunset = 1660763025
                ), visibility = 5000,
                weather = listOf(
                    Weather(
                        description = "light rain",
                        icon = "10d",
                        id = 500,
                        main = "Rain"
                    )
                ),
                wind = Wind(deg = 130, speed = 6.17), timezone = 0
            )
        )
    )
}