package com.ramprasad.weatherapp.db

import com.ramprasad.weatherapp.di.CountryData.getCountryList
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

/**
 * Created by Ramprasad on 6/7/23.
 */
@ExperimentalCoroutinesApi
class CountryDataSourceTest {

    private val countriesDao: CountriesDao = mockk(relaxed = true)
    private val countryDataSource = CountryDataSource(countriesDao)

    @Test
    fun insertCountry() = runBlockingTest {
        val country0 = getCountryList()[0]
        countryDataSource.insertCountry(country0)

        val country1 = getCountryList()[1]
        coVerify(exactly = 1) { countriesDao.insertCountry(country0) }
        coVerify(exactly = 0) { countriesDao.insertCountry(country1) }
    }

    @Test
    fun getCountries() = runBlockingTest {
        val countries = getCountryList()
        coEvery { countriesDao.getCountries() } returns countries

        val result = countryDataSource.getCountries()

        coVerify { countriesDao.getCountries() }
        assertEquals(result, countries)
    }
}