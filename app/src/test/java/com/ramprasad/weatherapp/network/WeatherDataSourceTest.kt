package com.ramprasad.weatherapp.network

import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

/**
 * Created by Ramprasad on 6/7/23.
 */
@ExperimentalCoroutinesApi
class WeatherDataSourceTest {

    private val weatherServiceApi: WeatherServiceApi = mockk(relaxed = true)
    private val weatherDataSource: WeatherDataSource = WeatherDataSource(weatherServiceApi)

    @Test
    fun getWeathersList() = runBlockingTest {
        val cityNames = "123,123"
        weatherDataSource.getWeathersList(cityNames).collect { }

        coVerify {
            weatherServiceApi.getWeathersList(cityNames)
        }
    }

    @Test
    fun getWeatherByCityName() = runBlockingTest {
        val cityName = "city_name"
        weatherDataSource.getWeatherByCityName(cityName).collect { }

        coVerify {
            weatherServiceApi.getWeatherByCityName(cityName)
        }
    }

    @Test
    fun getWeatherByLatLon() = runBlockingTest {
        val lat = "lat"
        val lon = "lon"
        weatherDataSource.getWeatherByLatLon(lat, lon).collect { }

        coVerify {
            weatherServiceApi.getWeatherByLatLon(lat, lon)
        }
    }
}