package com.ramprasad.weatherapp.repository

import com.ramprasad.weatherapp.db.CountryDataSource
import com.ramprasad.weatherapp.di.CountryData
import com.ramprasad.weatherapp.repositories.CountryRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.Assert
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

/**
 * Created by Ramprasad on 6/7/23.
 */
@ExperimentalCoroutinesApi
class CountryRepositoryTest {

    private val dataSource: CountryDataSource = mockk(relaxed = true)
    private val repository = CountryRepository(dataSource)

    @Test
    fun insertCountry() = runBlockingTest {
        val country0 = CountryData.getCountryList()[0]
        repository.insertCountry(country0)

        val country1 = CountryData.getCountryList()[1]
        coVerify { dataSource.insertCountry(country0) }
        coVerify(exactly = 0) { dataSource.insertCountry(country1) }
    }

    @Test
    fun getCountries() = runBlockingTest {
        val countries = CountryData.getCountryList()
        coEvery { dataSource.getCountries() } returns countries

        val result = repository.getCountries()

        coVerify { dataSource.getCountries() }
        Assert.assertEquals(result, countries)
    }
}