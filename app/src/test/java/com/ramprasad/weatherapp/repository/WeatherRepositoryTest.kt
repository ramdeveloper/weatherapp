package com.ramprasad.weatherapp.repository

import com.ramprasad.weatherapp.network.WeatherDataSource
import com.ramprasad.weatherapp.repositories.WeatherRepository
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

/**
 * Created by Ramprasad on 6/7/23.
 */
@ExperimentalCoroutinesApi
class WeatherRepositoryTest {

    private val dataSource: WeatherDataSource = mockk(relaxed = true)
    private var repository: WeatherRepository = WeatherRepository(dataSource)

    @Test
    fun getWeatherList() = runBlockingTest {
        val citiesIds = "123123,123123"
        repository.getWeathersList(citiesIds).collect { }

        coVerify {
            dataSource.getWeathersList(citiesIds)
        }
    }

    @Test
    fun getWeatherByCityName() = runBlockingTest {
        val cityName = "cityname"
        repository.getWeatherByCityName(cityName).collect { }

        coVerify {
            dataSource.getWeatherByCityName(cityName)
        }
    }

    @Test
    fun getWeatherByLatLon() = runBlockingTest {
        val lat = "lat"
        val lon = "lon"
        repository.getWeatherByLatLon(lat, lon).collect { }

        coVerify {
            dataSource.getWeatherByLatLon(lat, lon)
        }
    }
}