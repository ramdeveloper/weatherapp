package com.ramprasad.weatherapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ramprasad.weatherapp.di.CountryData.getCountryList
import com.ramprasad.weatherapp.repositories.CountryRepository
import com.ramprasad.weatherapp.repositories.WeatherRepository
import com.ramprasad.weatherapp.utils.WeatherFactory.createWeatherListNetworkResult
import com.ramprasad.weatherapp.utils.WeatherFactory.createWeatherNetworkResult
import com.ramprasad.weatherapp.utils.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.Assert
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test

/**
 * Created by Ramprasad on 6/7/23.
 */
@ExperimentalCoroutinesApi
class WeatherDataViewModelTest {
    private val weatherRepository: WeatherRepository = mockk()
    private val countryRepository: CountryRepository = mockk()
    private val viewModel = WeatherDataViewModel(weatherRepository, countryRepository)

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun getCountries() = runBlockingTest {
        val countries = getCountryList()
        coEvery { countryRepository.getCountries() } returns countries

        val result = viewModel.getCountries()

        coVerify { countryRepository.getCountries() }
        assertEquals(result, countries)
    }

    @Test
    fun getWeathers() = runBlockingTest {
        val citiesIds = "123,123"
        val countries = createWeatherListNetworkResult()
        val flowCountries = flow {
            emit(countries)
        }

        coEvery {
            weatherRepository.getWeathersList(citiesIds)
        } returns flowCountries

        viewModel.getWeathers(citiesIds)

        coVerify { weatherRepository.getWeathersList(citiesIds) }

        val result = viewModel.weathersList.getOrAwaitValue()

        assertEquals(result, countries)
    }

    @Test
    fun getWeatherByLatLon() = runBlockingTest {
        val lat = "lat"
        val lon = "lon"

        val country = createWeatherNetworkResult()
        val flowCountry = flow {
            emit(country)
        }

        coEvery {
            weatherRepository.getWeatherByLatLon(lat, lon)
        } returns flowCountry

        viewModel.getWeatherByLatLon(lat, lon)

        coVerify { weatherRepository.getWeatherByLatLon(lat, lon) }

        val result = viewModel.singleWeather.getOrAwaitValue()

        assertEquals(result, country)
        Assert.assertTrue(result.data?.isCurrentLocation ?: false)
    }

    @Test
    fun getWeatherByCityName() = runBlockingTest {
        val cityName = "cityName"

        val country = createWeatherNetworkResult()
        val flowCountry = flow {
            emit(country)
        }

        coEvery {
            weatherRepository.getWeatherByCityName(cityName)
        } returns flowCountry

        viewModel.getWeatherByCityName(cityName)

        coVerify { weatherRepository.getWeatherByCityName(cityName) }

        val result = viewModel.singleWeather.getOrAwaitValue()

        assertEquals(result, country)
    }
}